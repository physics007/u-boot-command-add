// SPDX-License-Identifier: GPL-2.0+
/*
 * Implements the 'bd' command to show board information
 *
 * (C) Copyright 2003
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 */

#include <common.h>
#include <command.h>
#include <env.h>
#include <lmb.h>
#include <net.h>
#include <vsprintf.h>
#include <asm/cache.h>

int do_bsimple(struct cmd_tbl *cmdtp, int flag, int argc, char *const argv[])
{
	printf("WelCome Volansys Technology\n");
	return 0;
}

U_BOOT_CMD(
	bsimple,	1,	1,	do_bsimple,
	"print simple test case",
	""
);

